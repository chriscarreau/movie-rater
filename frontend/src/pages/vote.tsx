import React, { useContext, useEffect, useState } from "react";
import { AuthContext } from "../contexts/authContext";
import { Head2Head } from "../models/head-2-head";
import HeadToHead from "../components/head-to-head";
import "./vote.css";

export default function Vote() {
  const authContext = useContext(AuthContext);
  const [H2Hs, setH2Hs] = useState<Head2Head[]>([]);
  const [currentH2H, setCurrentH2H] = useState<number>(0);

  async function fetchH2Hs() {
    if (authContext && authContext.authApiClient) {
      const response =
        await authContext.authApiClient.get<Head2Head[]>("/h2h/all");
      if (response.status === 200) {
        const filtered = response.data.filter((h2h) =>
          H2Hs.every((h) => h.h2h_id !== h2h.h2h_id),
        );
        setH2Hs(H2Hs.concat(filtered));
      }
    }
  }

  async function submitVote(h2hId: string, winnerId: number) {
    if (authContext && authContext.authApiClient) {
      const response = await authContext.authApiClient.post("/h2h/vote", {
        h2h_id: h2hId,
        vote: winnerId,
      });
      console.log(response);
      if (response.status === 200) {
        if (currentH2H < H2Hs.length) {
          setCurrentH2H(currentH2H + 1);
        } else {
          console.log("reached end");
        }
        if (currentH2H > H2Hs.length - 5) {
          await fetchH2Hs();
        }
      } else {
        console.error(response.data);
      }
    }
  }

  useEffect(() => {
    fetchH2Hs();
  }, []);

  return (
    <div className="flex flex-col max-w-7xl mx-auto mt-16">
      {H2Hs.length > 0 && currentH2H < H2Hs.length ? (
        <HeadToHead vote={submitVote} h2h={H2Hs[currentH2H]} />
      ) : null}
    </div>
  );
}
