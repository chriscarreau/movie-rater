import React, { useContext, useEffect, useState } from "react";
import { AuthContext } from "../contexts/authContext";
import { LeaderboardMovie } from "../models/leaderboard-movie";
import tmdbLogo from "../assets/tmdb_small.svg";
import starIcon from "../assets/star.svg";

export default function Leaderboard() {
  const authContext = useContext(AuthContext);
  const [leaderboard, setLeaderboard] = useState<LeaderboardMovie[]>([]);

  async function fetchLeaderboard() {
    if (authContext && authContext.authApiClient) {
      const response =
        await authContext.authApiClient.get<LeaderboardMovie[]>(
          "/h2h/leaderboard",
        );
      if (response.status === 200) {
        setLeaderboard(response.data.sort((a, b) => b.rating - a.rating));
      }
    }
  }

  function getYear(date: string): number {
    return new Date(date).getFullYear();
  }

  useEffect(() => {
    fetchLeaderboard();
  }, []);
  return (
    <div className="flex flex-col max-w-7xl mx-auto mt-16">
      <h2 className="mb-11 font-bold">YOUR TOP MOVIES</h2>
      <div className="flex flex-col">
        {leaderboard.map((mov, index) => {
          const isFirst = index === 0;
          return (
            <div
              key={mov.movie.id}
              className={
                "flex flex-row justify-between p-4 " +
                (isFirst ? "mb-8 bg-opacity-20 bg-black" : "")
              }
            >
              <div className="flex flex-row">
                <div className="w-24">
                  <span className={"text-5xl " + (isFirst ? "active" : "")}>
                    {index + 1}.
                  </span>
                </div>
                <div className="flex flex-row p-2">
                  <img
                    className={isFirst ? "w-48" : "w-24"}
                    alt={`poster for movie ${mov.movie.title}`}
                    src={
                      "http://image.tmdb.org/t/p/w342/" + mov.movie.poster_path
                    }
                  />
                  <div className="flex flex-col px-2 max-w-xl">
                    <div className="text-2xl uppercase font-bold">
                      {mov.movie.title}
                    </div>
                    <div>{getYear(mov.movie.release_date)}</div>
                    {isFirst ? (
                      <div className="mt-4">{mov.movie.overview}</div>
                    ) : null}
                  </div>
                </div>
              </div>
              <div className="flex flex-row">
                <div className="mr-8 text-center">
                  <img className="w-14" alt="tmdb logo" src={tmdbLogo} />
                  <div className="flex flex-row content-center justify-center">
                    <div>{mov.movie.vote_average.toFixed(1)}</div>
                    <div className="flex items-center">
                      <img className="w-4" alt="star icon" src={starIcon} />
                    </div>
                  </div>
                </div>
                <div className="flex flex-col">
                  <div>{mov.rating}</div>
                  <div className="flex flex-row text-gray-400">
                    <div className="mr-4 ">
                      {mov.amount_wins}
                      <span className="text-xs">W</span>
                    </div>
                    <div>
                      {mov.amount_loss}
                      <span className="text-xs">L</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
}
