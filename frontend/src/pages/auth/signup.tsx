import React, { useContext } from "react";
import axios from "axios";
import { AuthDispatchContext } from "../../contexts/authContext";
import { FieldValues, useForm } from "react-hook-form";
import { joiResolver } from "@hookform/resolvers/joi";
import * as joi from "joi";

const schema = joi
  .object({
    username: joi.string().required(),
    password: joi.string().required(),
    passwordConfirmation: joi.ref("password"),
  })
  .with("password", "passwordConfirmation");

export default function SignUp() {
  const authContext = useContext(AuthDispatchContext);
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: joiResolver(schema),
  });

  async function onSubmit(data: FieldValues) {
    try {
      const baseUrl = process.env.REACT_APP_API_URL;
      const response = await axios.post(`${baseUrl}/auth/signup`, {
        username: data.username,
        password: data.password,
      });
      if (response.status === 200) {
        const responseBody = response.data;
        console.log({ responseBody });
        if (authContext)
          authContext({
            type: "SET",
            authInfo: {
              username: data.username,
              bearerToken: responseBody.token,
              expiry: responseBody.expiry,
            },
          });
      }
    } catch (e) {
      console.log(e);
    }
  }

  return (
    <div className="flex flex-col max-w-lg mx-auto mt-16">
      <h2 className="text-center mb-11 font-bold">CREATE AN ACCOUNT</h2>
      <form className="flex flex-col gap-11" onSubmit={handleSubmit(onSubmit)}>
        <div className="flex flex-col gap-1">
          <label htmlFor="username">Username</label>
          <input
            type="text"
            {...register("username", { required: true })}
            placeholder="Username"
          />
        </div>
        <div className="flex flex-col gap-1">
          <label htmlFor="password">Password</label>
          <input
            type="password"
            {...register("password", { required: true })}
            placeholder="Password"
          />
        </div>
        <div className="flex flex-col gap-1">
          <label htmlFor="passwordConfirmation">Confirm Password</label>
          <input
            type="password"
            className={
              errors.passwordConfirmation ? "border-2 border-rose-500" : ""
            }
            {...register("passwordConfirmation", { required: true })}
            placeholder="Confirm Password"
          />
          {errors.passwordConfirmation && (
            <div className="text-sm text-rose-500">
              {errors.passwordConfirmation.message?.toString()}
            </div>
          )}
        </div>
        <button
          type="submit"
          className="btn-primary w-fit px-8 py-3 self-center"
        >
          SIGN UP
        </button>
      </form>
      <div className="text-sm text-center">
        <div>Already have an account ?</div>
        <a className="underline" href="/auth/login">
          Log In
        </a>
      </div>
    </div>
  );
}
