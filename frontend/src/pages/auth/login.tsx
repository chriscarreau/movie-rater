import React, { useContext, useEffect, useState } from "react";
import { AuthContext, AuthDispatchContext } from "../../contexts/authContext";
import { useNavigate } from "react-router-dom";
import axios from "axios";

export default function LogIn() {
  const authContext = useContext(AuthContext);
  const authDispatchContext = useContext(AuthDispatchContext);
  const navigate = useNavigate();
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  async function handleSubmit() {
    try {
      const baseUrl = process.env.REACT_APP_API_URL;
      const response = await axios.post(`${baseUrl}/auth/login`, {
        username: username,
        password: password,
      });
      if (response.status === 200) {
        const data = response.data;
        console.log({ data });
        if (authDispatchContext) {
          authDispatchContext({
            type: "SET",
            authInfo: {
              username: username,
              bearerToken: data.token,
              expiry: data.expiry,
            },
          });
          navigate("/vote");
        }
      }
    } catch (e) {
      console.log(e);
    }
  }

  useEffect(() => {
    if (authContext?.authInfo?.bearerToken) {
      navigate("/vote");
    }
  }, []);

  return (
    <div className="flex flex-col max-w-lg mx-auto mt-16">
      <h2 className="text-center mb-11 font-bold">LOG IN</h2>
      <form
        className="flex flex-col gap-11"
        onSubmit={(event) => {
          event.preventDefault();
          handleSubmit();
        }}
      >
        <div className="flex flex-col gap-1">
          <label htmlFor="username">Username</label>
          <input
            type="text"
            name="username"
            placeholder="Username"
            value={username}
            onChange={(event) => {
              setUsername(event.target.value);
            }}
            required
          />
        </div>
        <div className="flex flex-col gap-1">
          <label htmlFor="password">Password</label>
          <input
            type="password"
            name="password"
            placeholder="Password"
            value={password}
            onChange={(event) => {
              setPassword(event.target.value);
            }}
            required
          />
        </div>
        <button
          type="submit"
          className="btn-primary w-fit px-8 py-3 self-center"
        >
          LOG IN
        </button>
      </form>
      <div className="text-sm text-center">
        <div>Don&lsquo;t have an account ?</div>
        <a className="underline" href="/auth/signup">
          Create an account
        </a>
      </div>
    </div>
  );
}
