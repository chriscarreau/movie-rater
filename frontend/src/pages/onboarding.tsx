import React from "react";
import imgTMDB from "../assets/tmdb.svg";
import imgLetterboxd from "../assets/letterboxd.svg";

export default function Onboarding() {
  const handleClick = (test: string) => {
    console.log(test);
  };
  return (
    <main className="container mx-auto mt-20 flex flex-col gap-y-20 h-full">
      <div className="mx-auto w-fit">
        <h1 className="font-bold">SELECT A MOVIE SOURCE</h1>
      </div>
      <div className="flex flex-auto flex-row justify-center align-items-center gap-x-10">
        <div
          onClick={() => handleClick("aaa")}
          className="flex flex-col mt-20 items-center gap-y-10 hover:cursor-pointer"
        >
          <div>
            <h2>TMDB Top 100 Movies</h2>
          </div>
          <div>
            <img alt="TMDB logo" src={imgTMDB} width={180} />
          </div>
        </div>
        <div className="w-1 bg-orange-400 h-96"></div>
        <div
          onClick={() => handleClick("bb")}
          className="flex flex-col mt-20 items-center gap-y-10 hover:cursor-pointer"
        >
          <div>
            <h2>Letterboxd profile</h2>
          </div>
          <div>
            <img alt="Letterboxd logo" src={imgLetterboxd} width={300} />
          </div>
        </div>
        <div className="w-1 bg-orange-400 h-96"></div>
        <div className="flex flex-col mt-20 items-center gap-y-10">
          <div>
            <h2>Custom list of movies</h2>
          </div>
          <div>
            <textarea
              placeholder="id1, id2, id3, ..."
              rows={5}
              cols={18}
            ></textarea>
          </div>
          <div>
            <button className="btn-primary">GO!</button>
          </div>
        </div>
      </div>
    </main>
  );
}
