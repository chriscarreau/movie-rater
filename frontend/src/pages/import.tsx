import React, { useContext, useState } from "react";
import { AuthContext } from "../contexts/authContext";
import { LetterboxdCSVData, LetterboxdData } from "../models/letterboxd-data";
import Papa from "papaparse";
import imgTMDB from "../assets/tmdb.svg";
import imgLetterboxd from "../assets/letterboxd.svg";
import DragNDropInput from "../components/drag-n-drop/drag-n-drop-input";

import "./import.css";
import fileIcon from "../assets/file.svg";
import trashIcon from "../assets/trash.svg";
import successIcon from "../assets/success.svg";
import errorIcon from "../assets/error.svg";
import spinner from "../assets/spinner.svg";

type PageState = "default" | "importing" | "success" | "error";

export default function Import() {
  const authContext = useContext(AuthContext);
  const [lbData, setLbData] = useState<LetterboxdData[]>();
  const [uploadedFile, setUploadedFile] = useState<File>();
  const [pagestate, setPagestate] = useState<PageState>("default");

  function deleteFile(): void {
    setLbData(undefined);
    setUploadedFile(undefined);
  }

  function parseCSVintoLetterboxdData(csv: string): LetterboxdData[] {
    const parsed = Papa.parse<LetterboxdCSVData>(csv, {
      header: true, // Set to true if the CSV contains headers
      skipEmptyLines: true, // Skip empty rows
    });
    return parsed.data.map((row) => {
      return { title: row.Name, year: parseInt(row.Year) };
    });
  }

  async function readFile(files: File[]) {
    setUploadedFile(files[0]);
    const reader = new FileReader();
    reader.onload = (e: ProgressEvent<FileReader>) => {
      const fileContent = e.target?.result;
      if (fileContent && typeof fileContent === "string") {
        const letterboxData = parseCSVintoLetterboxdData(fileContent);
        setLbData(letterboxData);
      }
    };
    reader.readAsText(files[0]);
  }

  async function uploadLetterboxdData() {
    if (authContext?.authApiClient && lbData) {
      setPagestate("importing");
      try {
        await authContext?.authApiClient.post(
          "/movie/import/letterboxd",
          lbData,
        );
        setPagestate("success");
        console.log("upload successful");
      } catch (e) {
        setPagestate("error");
        console.error("Error uploading data", e);
      }
    }
  }

  async function importTMDBTop100() {
    if (authContext?.authApiClient) {
      setPagestate("importing");
      try {
        await authContext?.authApiClient.post("/movie/import/tmdb");
        setPagestate("success");
        console.log("upload successful");
      } catch (e) {
        setPagestate("error");
        console.error("Error uploading data", e);
      }
    }
  }

  function Spinner() {
    return (
      <div className="flex items-center content-center h-screen">
        <img src={spinner} />
      </div>
    );
  }

  function LetterboxdUploader() {
    return (
      <div className="flex flex-col mt-20 items-center gap-y-10 ">
        <div>
          <h2>Letterboxd profile</h2>
        </div>
        <div>
          <img alt="Letterboxd logo" src={imgLetterboxd} width={200} />
        </div>
        {lbData ? (
          <div>
            <div className="file-list w-full">
              <div className="flex flex-row justify-between">
                <div className="flex flex-row">
                  <img src={fileIcon}></img>
                  {uploadedFile?.name}
                </div>
                <img
                  className="cursor-pointer"
                  onClick={deleteFile}
                  src={trashIcon}
                ></img>
              </div>
            </div>
            <div>Found {lbData.length} movies ready for import</div>
          </div>
        ) : (
          <DragNDropInput onFilesSelected={readFile} />
        )}
        <div>
          <button
            onClick={uploadLetterboxdData}
            className="btn-primary"
            disabled={!lbData ? true : false}
          >
            IMPORT
          </button>
        </div>
      </div>
    );
  }

  function TMDBUploader() {
    return (
      <div className="flex flex-col mt-20 items-center gap-y-10 hover:cursor-pointer">
        <div>
          <h2>TMDB Top 100 Movies</h2>
        </div>
        <div>
          <img alt="TMDB logo" src={imgTMDB} width={150} />
        </div>
        <div>
          This will add the top 100 movies according
          <br /> to TMDB users to your account
        </div>
        <div>
          <button onClick={importTMDBTop100} className="btn-primary">
            IMPORT
          </button>
        </div>
      </div>
    );
  }

  function MainContent() {
    return (
      <div>
        <div className="mx-auto w-fit">
          <h1 className="font-bold">Select your movie source</h1>
        </div>
        <div className="flex flex-auto flex-row justify-center align-items-center gap-x-10">
          <TMDBUploader />
          <div className="w-0 border-r-2 border-orange-200"></div>
          <LetterboxdUploader />
        </div>
      </div>
    );
  }

  function ContentSwitcher() {
    switch (pagestate) {
      case "importing":
        return <Spinner />;
      case "success":
        return (
          <div>
            <img src={successIcon} />
          </div>
        );
      case "error":
        return (
          <div>
            <img src={errorIcon} />
          </div>
        );
      default:
        return <MainContent />;
    }
  }

  return (
    <main className="container mx-auto mt-20 flex flex-col gap-y-20 h-full">
      <ContentSwitcher />
    </main>
  );
}
