import axios, { AxiosInstance, InternalAxiosRequestConfig } from "axios";
import React, { createContext, Dispatch, useReducer } from "react";

const AUTH_KEY = "auth";

export type AuthInfo = {
  username: string;
  bearerToken: string;
  expiry: number;
};

type AuthContextData = {
  authInfo?: AuthInfo;
  authApiClient?: AxiosInstance;
};

export type AuthInfoAction = {
  type: "SET" | "DELETE";
  authInfo?: AuthInfo;
};

type AuthProviderProps = {
  children: JSX.Element | JSX.Element[];
};

export const AuthContext = createContext<AuthContextData | undefined>(
  undefined,
);
export const AuthDispatchContext = createContext<
  Dispatch<AuthInfoAction> | undefined
>(undefined);

export const AuthProvider = ({ children }: AuthProviderProps) => {
  const authString = localStorage.getItem(AUTH_KEY);
  let startupAuthContext: AuthContextData | undefined;
  if (authString) {
    const auth = JSON.parse(authString) as AuthInfo;
    const now = new Date();
    if (now.valueOf() < auth.expiry * 1000) {
      const client = createAuthApiClient(auth.bearerToken);
      startupAuthContext = {
        authInfo: auth,
        authApiClient: client,
      };
    } else {
      localStorage.removeItem(AUTH_KEY);
    }
  }
  const [authInfo, dispatch] = useReducer(authReducer, startupAuthContext);
  return (
    <AuthContext.Provider value={authInfo}>
      <AuthDispatchContext.Provider value={dispatch}>
        {children}
      </AuthDispatchContext.Provider>
    </AuthContext.Provider>
  );
};

function authReducer(
  authInfo: AuthContextData | undefined,
  action: AuthInfoAction,
): AuthContextData | undefined {
  switch (action.type) {
    case "SET": {
      if (action.authInfo) {
        localStorage.setItem(AUTH_KEY, JSON.stringify(action.authInfo));
      }
      return { ...authInfo, authInfo: action.authInfo };
    }
    case "DELETE": {
      localStorage.removeItem(AUTH_KEY);
      return undefined;
    }
    default: {
      throw Error("Unknown action: " + action.type);
    }
  }
}

function createAuthApiClient(bearerToken: string): AxiosInstance {
  const client = axios.create({ baseURL: process.env.REACT_APP_API_URL });
  client.interceptors.request.use((config: InternalAxiosRequestConfig) => {
    config.headers.Authorization = `Bearer ${bearerToken}`;
    return config;
  });
  return client;
}
