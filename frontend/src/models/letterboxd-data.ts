export type LetterboxdData = {
  title: string;
  year: number;
};

export type LetterboxdCSVData = {
  Date: string;
  Name: string;
  Year: string;
  "Letterboxd URI": string;
};
