import { Movie } from "./movie";

export type Head2Head = {
  h2h_id: string;
  movie1: Movie;
  movie2: Movie;
};
