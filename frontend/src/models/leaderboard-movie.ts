import { Movie } from "./movie";

export type LeaderboardMovie = {
  movie: Movie;
  amount_wins: number;
  amount_loss: number;
  rating: number;
};
