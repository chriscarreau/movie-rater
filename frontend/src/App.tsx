import React from "react";
import "./App.css";
import { createBrowserRouter, Outlet, RouterProvider } from "react-router-dom";
import Onboarding from "./pages/onboarding";
import LogIn from "./pages/auth/login";
import SignUp from "./pages/auth/signup";
import Leaderboard from "./pages/leaderboard";
import Vote from "./pages/vote";
import { AuthProvider } from "./contexts/authContext";
import Header from "./components/header";
import { ProtectedRoute } from "./components/protected-route";
import Import from "./pages/import";

function Layout() {
  return (
    <div className="App h-full">
      <Header />
      <div className="Content">
        <Outlet /> {/* Renders the current route's element */}
      </div>
    </div>
  );
}

export default function App() {
  const router = createBrowserRouter([
    {
      path: "/",
      element: <Layout />, // Wrap all routes in the Layout component
      children: [
        { path: "/", element: <Onboarding /> },
        { path: "/auth/login", element: <LogIn /> },
        { path: "/auth/signup", element: <SignUp /> },
        {
          path: "/vote",
          element: (
            <ProtectedRoute>
              <Vote />
            </ProtectedRoute>
          ),
        },
        {
          path: "/leaderboard",
          element: (
            <ProtectedRoute>
              <Leaderboard />
            </ProtectedRoute>
          ),
        },
        {
          path: "/import",
          element: (
            <ProtectedRoute>
              <Import />
            </ProtectedRoute>
          ),
        },
      ],
    },
  ]);
  return (
    <AuthProvider>
      <RouterProvider router={router} />
    </AuthProvider>
  );
}
