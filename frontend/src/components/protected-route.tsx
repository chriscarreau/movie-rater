import React, { PropsWithChildren, useContext, useEffect } from "react";
import { AuthContext } from "../contexts/authContext";
import { useNavigate } from "react-router-dom";

export function ProtectedRoute({ children }: PropsWithChildren) {
  const authData = useContext(AuthContext);
  const navigate = useNavigate();

  useEffect(() => {
    if (!authData?.authInfo?.bearerToken) {
      // user is not authenticated
      navigate("/auth/login");
    }
  }, []);
  return <div>{children}</div>;
}
