import React, { ChangeEvent, useEffect, useRef, useState } from "react";
import "./drag-n-drop.css";

type DragNDropInputProps = {
  onFilesSelected: (files: File[]) => void;
};

export default function DragNDropInput({
  onFilesSelected,
}: DragNDropInputProps) {
  const [files, setFiles] = useState<File[]>([]);
  const fileInputRef = useRef<HTMLInputElement | null>(null);

  const handleClick = () => {
    fileInputRef.current?.click();
  };

  const handleFileChange = (event: ChangeEvent<HTMLInputElement>) => {
    const selectedFiles = event.target.files;
    if (selectedFiles && selectedFiles.length > 0) {
      const newFiles = Array.from(selectedFiles);
      setFiles((prevFiles) => [...prevFiles, ...newFiles]);
    }
  };
  const handleDrop = (event: React.DragEvent) => {
    event.preventDefault();
    const droppedFiles = event.dataTransfer.files;
    if (droppedFiles.length > 0) {
      const newFiles = Array.from(droppedFiles);
      setFiles((prevFiles) => [...prevFiles, ...newFiles]);
    }
  };

  useEffect(() => {
    if (!files || files.length === 0) {
      return;
    }
    onFilesSelected(files);
  }, [files, onFilesSelected]);

  return (
    <div
      onDrop={handleDrop}
      onDragOver={(event) => event.preventDefault()}
      onClick={handleClick}
      className="drop-box"
    >
      <input
        type="file"
        hidden
        id="browse"
        onChange={handleFileChange}
        accept=".csv"
        ref={fileInputRef}
      />
      <div className="text-lg font-bold text-gray-300">
        Drag your Letterboxd file here
      </div>
      <div className="text-sm text-gray-400">
        or click ot open the file selector
      </div>
    </div>
  );
}
