import React, { useContext, useEffect, useState } from "react";
import imgLogo from "../assets/icon.png";
import imgUser from "../assets/user.svg";
import { AuthContext, AuthDispatchContext } from "../contexts/authContext";
import { useLocation, useNavigate } from "react-router-dom";

export default function Header() {
  const location = useLocation();
  const navigate = useNavigate();
  const currentPath = location.pathname;
  const authContext = useContext(AuthContext);
  const authDispatchContext = useContext(AuthDispatchContext);
  const [username, setUsername] = useState("");
  const [showUserMenu, setShowUserMenu] = useState(false);
  useEffect(() => {
    if (authContext && authContext.authInfo) {
      setUsername(authContext.authInfo.username);
    }
  }, []);

  function getActiveClass(path: string) {
    console.log(`currentPath: ${currentPath}, path: ${path}`);
    return currentPath === path ? "active" : "";
  }

  function toggleMenu() {
    setShowUserMenu(!showUserMenu);
  }

  function logout() {
    if (!authDispatchContext) {
      return;
    }
    authDispatchContext({
      type: "DELETE",
    });
    navigate("/");
  }

  return (
    <header className="container h-[4REM] mx-auto flex flex-row items-center justify-between">
      <div className="flex flex-row items-center">
        <div className="mr-10">
          <img alt="Movie-rater Logo" src={imgLogo} width={40} />
        </div>
        <nav>
          <ul className="flex h-full font-bold">
            <a href="/vote">
              <li
                className={
                  "hover:cursor-pointer mr-10 " + getActiveClass("/vote")
                }
              >
                VOTE
              </li>
            </a>
            <a href="/leaderboard">
              <li
                className={
                  "hover:cursor-pointer mr-10 " + getActiveClass("/leaderboard")
                }
              >
                LEADERBOARD
              </li>
            </a>
            <a href="/import">
              <li
                className={
                  "hover:cursor-pointer mr-10 " + getActiveClass("/import")
                }
              >
                IMPORT
              </li>
            </a>
          </ul>
        </nav>
      </div>
      <div className="flex flex-row items-center">
        <div className="mr-2">{username}</div>
        <div
          className="hover:cursor-pointer relative"
          onClick={() => toggleMenu()}
        >
          <img alt="User Account icon" src={imgUser} width={40} />
          <div
            onClick={() => logout()}
            className={
              showUserMenu
                ? "absolute rounded w-20 top-11 right-0 p-2 bg-neutral-800"
                : "hidden"
            }
          >
            log out
          </div>
        </div>
      </div>
    </header>
  );
}
