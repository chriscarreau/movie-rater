import React from "react";
import { Movie } from "../models/movie";

export type Side = "LEFT" | "RIGHT";
type MovieCardProps = {
  side: Side;
  movie: Movie;
};

export default function MovieCard(props: MovieCardProps) {
  function getYear(date: string): number {
    return new Date(date).getFullYear();
  }
  return (
    <div className={props.side === "LEFT" ? "text-left" : "text-right"}>
      <div className="min-h-28">
        <div className="mb-2">
          <h2 className="uppercase font-bold">{props.movie.original_title}</h2>
          {props.movie.original_title !== props.movie.title ? (
            <h4 className="uppercase font-bold">{props.movie.title}</h4>
          ) : null}
        </div>
        <div>
          <h3>{getYear(props.movie.release_date)}</h3>
        </div>
      </div>
      <div>
        <img
          alt={`poster for movie ${props.movie.title}`}
          src={"http://image.tmdb.org/t/p/w500/" + props.movie.poster_path}
        />
      </div>
    </div>
  );
}
