import React from "react";
import { Head2Head } from "../models/head-2-head";
import MovieCard from "./movie-card";

type HeadToHeadProps = {
  h2h: Head2Head;
  vote: (h2hId: string, winner: number) => void;
};

export default function HeadToHead(props: HeadToHeadProps) {
  return (
    <div className="flex flex-row justify-between">
      <div className="flex flex-col content-center h2h-side">
        <MovieCard movie={props.h2h.movie1} side="LEFT" />
        <button
          onClick={() => props.vote(props.h2h.h2h_id, props.h2h.movie1.id)}
          className="btn-primary w-full mt-4 px-8 py-3 self-center text-4xl"
        >
          VOTE
        </button>
      </div>
      <div className="w-0 border-r-2 border-orange-200"></div>
      <div className="flex flex-col content-center h2h-side">
        <MovieCard movie={props.h2h.movie2} side="RIGHT" />
        <button
          onClick={() => props.vote(props.h2h.h2h_id, props.h2h.movie2.id)}
          className="btn-primary w-full mt-4 px-8 py-3 self-center text-4xl"
        >
          VOTE
        </button>
      </div>
    </div>
  );
}
