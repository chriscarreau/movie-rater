-- Add migration script here

CREATE TABLE "user_movie"
(
    user_movie_id   uuid primary key default uuid_generate_v1mc(),
    user_id         uuid not null references "user" (user_id) on delete cascade,
    movie_id        integer not null,
    elo             double precision not null DEFAULT 1500,
    created_at      timestamptz not null default now()
);