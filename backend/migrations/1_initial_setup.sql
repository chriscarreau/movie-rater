-- Add migration script here

CREATE extension IF NOT EXISTS "uuid-ossp";

CREATE collation case_insensitive (provider = icu, locale = 'und-u-ks-level2', deterministic = false);

CREATE TABLE "user"
(
    user_id         uuid primary key default uuid_generate_v1mc(),
    username        text collate "case_insensitive" unique not null,
    password_hash   text not null,
    letterboxd_id   text,
    created_at      timestamptz not null default now()
);

CREATE TABLE head2head
(
    h2h_id          uuid primary key default uuid_generate_v1mc(),
    user_id         uuid not null references "user" (user_id) on delete cascade,
    movie1_id       integer not null,
    movie2_id       integer not null,
    winner          integer,
    created_at      timestamptz not null default now()
);