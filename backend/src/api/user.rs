use crate::api::{extractor::{get_jwt_expiry_time, AuthUser}, ApiContext, Error, Result, ResultExt};
use argon2::{password_hash::{rand_core::OsRng, SaltString, PasswordHasher}, Argon2, PasswordHash, PasswordVerifier};
use axum::{extract::State, routing::{get, post}, Json, Router};
use axum_macros::debug_handler;
use serde::{Deserialize, Serialize};

pub(crate) fn router() -> Router<ApiContext> {
    Router::new()
        .route("/api/auth/login", post(login))
        .route("/api/auth/signup", post(signup))
        .route("/api/auth/test", get(test))
}

#[derive(Deserialize, Serialize, Debug)]
struct User {
	user_id: String,
	username: String,
    letterboxd_id: Option<String>,
}

#[derive(Deserialize, Serialize, Debug)]
struct AuthReq {
    username: String,
    password: String,
}

#[derive(Deserialize, Serialize, Debug)]
struct AuthResponse {
    token: String,
    expiry: usize,
}



#[debug_handler]
async fn login(ctx: State<ApiContext>, Json(body): Json<AuthReq>) -> Result<Json<AuthResponse>> {
    log::info!("[login] Trying to log user with username: {:?}", body.username);
    let user = sqlx::query!(
        r#"SELECT user_id, username, password_hash, letterboxd_id FROM "user" WHERE username = $1"#,
        body.username
    )
    .fetch_one(&ctx.db)
    .await?;
    log::info!("[login] Found user in database with username: {:?}", user.username);

    let is_valid = match PasswordHash::new(&user.password_hash) {
        Ok(parsed_hash) => Argon2::default()
            .verify_password(body.password.as_bytes(), &parsed_hash)
            .map_or(false, |_| true),
        Err(_) => false,
    };

    if !is_valid {
        log::error!("[login] Failed login attempt: password didn't match for user: {:?}", user.username);
        return Err(Error::unprocessable_entity([("Invalid Login", "Invalid username or password")]));
    }

    // Make token
    let now = chrono::Utc::now();
    let token = AuthUser { user_id: user.user_id.to_string() }.to_jwt(&ctx);
    let expiry = get_jwt_expiry_time(now, &ctx);
    Ok(Json(AuthResponse { token, expiry }))
}

#[debug_handler]
async fn signup(ctx: State<ApiContext>, Json(body): Json<AuthReq>) -> Result<Json<AuthResponse>> {
    log::info!("[signup] Signing up user in database with username: {:?}", body.username);
    let salt = SaltString::generate(&mut OsRng);
    let password_hash = Argon2::default().hash_password(body.password.as_bytes(), &salt)?.to_string();    

    sqlx::query!(
        r#"INSERT INTO "user" (username, password_hash) VALUES ($1, $2)"#,
        body.username,
        password_hash
    )
    .execute(&ctx.db)
    .await
    .on_constraint("user_username_key", |_| {
        log::error!("[signup] username already taken");
        Error::unprocessable_entity([("username", "already taken")])
    })?;

    // Ok(Json(AuthResponse { token: String::from("success")}))
    login(ctx, Json(body)).await
}

#[debug_handler]
async fn test(auth_user: AuthUser, _: State<ApiContext>) -> Result<String> {
    log::info!("Authorized user {}", auth_user.user_id);
    Ok(String::from("user authorized successfully"))
}