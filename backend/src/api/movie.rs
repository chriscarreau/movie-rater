use crate::api::{self, ApiContext, Result};
use axum::{
  extract::{Path, State},
  routing::{get, post},
  Json, Router,
};
use axum_macros::debug_handler;
use rand::seq::SliceRandom;
use serde::{Deserialize, Serialize};
use std::str::FromStr;
use uuid::Uuid;

use super::extractor::AuthUser;

const MIN_VOTE: f64 = 3.0;
const MEAN: f64 = 0.5;

pub(crate) fn router() -> Router<ApiContext> {
  Router::new()
    .route("/api/h2h/all", get(get_user_h2hs))
    .route("/api/h2h/vote", post(vote_h2h))
    .route("/api/h2h/:id", get(get_h2h))
    .route("/api/h2h/leaderboard", get(get_user_leaderboard))
    .route(
      "/api/movie/import/letterboxd",
      post(import_letterboxd_movies),
    )
    .route("/api/movie/import/tmdb", post(import_tmdb_movies))
}

#[derive(Deserialize, Serialize, Debug, Clone)]
struct Movie {
  id: i32,
  imdb_id: String,
  original_language: String,
  original_title: String,
  overview: String,
  poster_path: String,
  release_date: String,
  runtime: i32,
  tagline: String,
  title: String,
  vote_average: f64,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct H2HMovie {
  pub id: i32,
  pub original_language: String,
  pub original_title: String,
  pub overview: String,
  pub poster_path: String,
  pub release_date: String,
  pub title: String,
  pub vote_average: f64,
}

impl From<Movie> for H2HMovie {
  fn from(movie: Movie) -> Self {
    H2HMovie {
      id: movie.id,
      original_language: movie.original_language,
      original_title: movie.original_title,
      overview: movie.overview,
      poster_path: movie.poster_path,
      release_date: movie.release_date,
      title: movie.title,
      vote_average: movie.vote_average,
    }
  }
}

#[derive(Deserialize, Serialize, Debug)]
struct TopMoviesResp {
  page: i32,
  results: Vec<H2HMovie>,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
struct Head2Head {
  h2h_id: String,
  movie1: H2HMovie,
  movie2: H2HMovie,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
struct Head2HeadDBRow {
  h2h_id: String,
  movie1_id: i32,
  movie2_id: i32,
  winner: Option<i32>,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
struct LeaderboardMovie {
  movie: H2HMovie,
  amount_wins: usize,
  amount_loss: usize,
  rating: f64,
}

#[derive(Deserialize, Serialize, Debug)]
struct Head2HeadVote {
  h2h_id: String,
  vote: i32,
}

#[derive(Deserialize, Serialize, Debug)]
struct UserMovie {
  user_movie_id: String,
  user_id: String,
  movie_id: i32,
  elo: f64,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
struct LetterboxdMovieData {
  title: String,
  year: i32,
}

#[derive(Deserialize, Serialize, Debug, Clone)]
struct SearchMovieResponse {
  page: i32,
  results: Vec<H2HMovie>,
}

async fn get_user_h2hs(
  auth_user: AuthUser,
  ctx: State<ApiContext>,
) -> Result<Json<Vec<Head2Head>>> {
  let mut head2heads: Vec<Head2Head> = Vec::new();
  // 1. fetch all users H2H with no votes
  let query_response = sqlx::query!(
    r#"SELECT h2h_id, movie1_id, movie2_id, winner FROM "head2head" WHERE user_id = $1 AND winner IS NULL LIMIT 20;"#,
    Uuid::from_str(&auth_user.user_id).unwrap(),
  )
  .fetch_all(&ctx.db)
  .await?;

  // 2. If not empty, fetch movie info for all the h2h and return the h2hs immediately
  if query_response.len() > 0 {
    for record in query_response {
      let movie1 = fetch_tmdb_movie(record.movie1_id, &ctx).await?;
      let movie2 = fetch_tmdb_movie(record.movie2_id, &ctx).await?;
      let h2h = Head2Head {
        h2h_id: record.h2h_id.clone().to_string(),
        movie1: movie1,
        movie2: movie2,
      };
      head2heads.push(h2h);
    }
    return Ok(Json(head2heads));
  }

  // 3. Else, fetch user movies and try to generate new H2H
  let user_movies = fetch_user_movies(auth_user.user_id.clone(), &ctx).await?;
  let mut user_movie_detailed: Vec<H2HMovie> = vec![];
  for user_movie in user_movies {
    let movie = fetch_tmdb_movie(user_movie.movie_id, &ctx).await?;
    user_movie_detailed.push(movie);
  }
  let mut existing_h2h: Vec<Head2Head> = Vec::new();

  let existing_h2h_records = sqlx::query!(
    r#"SELECT h2h_id, movie1_id, movie2_id, winner FROM "head2head" WHERE user_id = $1 AND winner IS NOT NULL"#,
    Uuid::from_str(&auth_user.user_id).unwrap(),
  )
  .fetch_all(&ctx.db)
  .await?;

  for record in existing_h2h_records {
    let movie1 = fetch_tmdb_movie(record.movie1_id, &ctx).await?;
    let movie2 = fetch_tmdb_movie(record.movie2_id, &ctx).await?;
    let h2h = Head2Head {
      h2h_id: record.h2h_id.clone().to_string(),
      movie1: movie1,
      movie2: movie2,
    };
    existing_h2h.push(h2h);
  }
  head2heads = generate_h2h(
    user_movie_detailed,
    existing_h2h,
    &ctx,
    Uuid::from_str(&auth_user.user_id).unwrap(),
  )
  .await?;

  Ok(Json(head2heads))
}

async fn get_user_leaderboard(
  auth_user: AuthUser,
  ctx: State<ApiContext>,
) -> Result<Json<Vec<LeaderboardMovie>>> {
  let mut leaderboard: Vec<LeaderboardMovie> = vec![];
  // 1. fetch all users H2H
  let existing_h2h_records: Vec<Head2HeadDBRow> = sqlx::query!(
    r#"SELECT h2h_id, movie1_id, movie2_id, winner FROM "head2head" WHERE user_id = $1 AND winner IS NOT NULL"#,
    Uuid::from_str(&auth_user.user_id).unwrap(),
  )
  .fetch_all(&ctx.db)
  .await?
  .into_iter()
  .map(|record| Head2HeadDBRow {
      h2h_id: record.h2h_id.to_string(),
      movie1_id: record.movie1_id,
      movie2_id: record.movie2_id,
      winner: record.winner,
  })
  .collect();

  // 2. fetch all user_movies
  let user_movies = fetch_user_movies(auth_user.user_id, &ctx).await?;

  // 3. fetch info for all movies
  for mov in user_movies {
    let movie_details = fetch_tmdb_movie(mov.movie_id, &ctx).await?;
    let all_movie_matchups: Vec<Head2HeadDBRow> = existing_h2h_records
      .iter()
      .filter(|&h2h| h2h.movie1_id == mov.movie_id || h2h.movie2_id == mov.movie_id)
      .cloned()
      .collect();
    let total_matchup = all_movie_matchups.len();
    // If a movie does not have enough votes, we exclude it from the list
    if (total_matchup as f64) < MIN_VOTE {
      continue;
    }
    let wins = all_movie_matchups
      .iter()
      .filter(|&h2h| h2h.winner == Some(mov.movie_id))
      .count();
    let losses = total_matchup - wins;
    leaderboard.push(LeaderboardMovie {
      movie: movie_details,
      amount_loss: losses,
      amount_wins: wins,
      rating: mov.elo,
    });
  }

  // 3. return all movies with their percentage
  Ok(Json(leaderboard))
}

async fn get_h2h(
  auth_user: AuthUser,
  ctx: State<ApiContext>,
  Path(id): Path<String>,
) -> Result<Json<Head2Head>> {
  let h2h = sqlx::query!(
        r#"SELECT h2h_id, movie1_id, movie2_id, winner FROM "head2head" WHERE h2h_id = $1 AND user_id = $2"#,
        Uuid::from_str(&id).unwrap(),
        Uuid::from_str(&auth_user.user_id).unwrap(),
    )
    .fetch_one(&ctx.db)
    .await?;
  let movie1 = fetch_tmdb_movie(h2h.movie1_id, &ctx).await?;
  let movie2 = fetch_tmdb_movie(h2h.movie2_id, &ctx).await?;
  Ok(Json(Head2Head {
    h2h_id: h2h.h2h_id.to_string(),
    movie1,
    movie2,
  }))
}

async fn vote_h2h(
  auth_user: AuthUser,
  ctx: State<ApiContext>,
  Json(h2h_vote): Json<Head2HeadVote>,
) -> Result<Json<()>> {
  let h2h_record = sqlx::query!(
        r#"SELECT h2h_id, movie1_id, movie2_id, winner FROM "head2head" WHERE h2h_id = $1 AND user_id = $2"#,
        Uuid::from_str(&h2h_vote.h2h_id).unwrap(),
        Uuid::from_str(&auth_user.user_id).unwrap(),
    )
    .fetch_one(&ctx.db)
    .await?;
  let h2h = Head2HeadDBRow {
    h2h_id: h2h_record.h2h_id.to_string(),
    movie1_id: h2h_record.movie1_id,
    movie2_id: h2h_record.movie2_id,
    winner: Some(h2h_vote.vote),
  };
  update_elo(h2h, &auth_user.user_id, &ctx).await?;
  sqlx::query!(
    r#"UPDATE "head2head" SET winner=$1 WHERE h2h_id = $2 AND user_id = $3"#,
    h2h_vote.vote,
    Uuid::from_str(&h2h_vote.h2h_id).unwrap(),
    Uuid::from_str(&auth_user.user_id).unwrap(),
  )
  .execute(&ctx.db)
  .await?;
  Ok(Json(()))
}

async fn fetch_user_movies(user_id: String, ctx: &State<ApiContext>) -> Result<Vec<UserMovie>> {
  // 1. fetch all users H2H with no votes
  let user_movies = sqlx::query!(
    r#"SELECT user_movie_id, user_id, movie_id, elo FROM "user_movie" WHERE user_id = $1"#,
    Uuid::from_str(&user_id).unwrap(),
  )
  .fetch_all(&ctx.db)
  .await?
  .iter()
  .map(|record| UserMovie {
    user_movie_id: record.user_movie_id.to_string(),
    movie_id: record.movie_id,
    user_id: record.user_id.to_string(),
    elo: record.elo,
  })
  .collect();

  Ok(user_movies)
}

#[axum_macros::debug_handler]
async fn import_letterboxd_movies(
  auth_user: AuthUser,
  ctx: State<ApiContext>,
  Json(letterboxd_data): Json<Vec<LetterboxdMovieData>>,
) -> Result<Json<()>> {
  for lb_data in letterboxd_data {
    let movie = search_tmdb_movie_by_title_and_year(lb_data.title, lb_data.year, &ctx).await;
    if let Ok(mov) = movie {
      log::info!(
        "Inserting movie '{}' in user_movie for user {}",
        mov.title,
        &auth_user.user_id
      );
      sqlx::query!(
        r#"INSERT INTO "user_movie" (user_id, movie_id) 
            SELECT $1, $2
            WHERE 
              NOT EXISTS (
                  SELECT user_movie_id FROM "user_movie" WHERE user_id = $1 AND movie_id = $2
              )"#,
        Uuid::from_str(&auth_user.user_id).unwrap(),
        mov.id
      )
      .execute(&ctx.db)
      .await?;
    }
  }
  Ok(Json(()))
}

async fn import_tmdb_movies(auth_user: AuthUser, ctx: State<ApiContext>) -> Result<()> {
  let tmdb_movies = get_top100_movies(&ctx).await?;
  for movie in tmdb_movies {
    let _result = sqlx::query!(
      r#"INSERT INTO "user_movie" (user_id, movie_id) 
            SELECT $1, $2
            WHERE 
              NOT EXISTS (
                  SELECT user_movie_id FROM "user_movie" WHERE user_id = $1 AND movie_id = $2
              )
            RETURNING user_movie_id "#,
      Uuid::from_str(&auth_user.user_id).unwrap(),
      movie.id
    )
    .fetch_one(&ctx.db)
    .await?;
  }
  Ok(())
}

/// Helper function to fetch a movie from the TMDB API by ID.
/// Uses an in-memory cache to avoid fetching all the movies
///
/// ### Arguments
/// id: The ID of the movie to fetch.
/// ctx: The current API context.
///
/// ### Returns
/// A `H2HMovie` object representing the movie.
async fn fetch_tmdb_movie(id: i32, ctx: &State<ApiContext>) -> Result<H2HMovie> {
  let mut cache = ctx.movie_cache.lock().await;
  if let Some(movie) = cache.get(&id) {
    return Ok(movie.clone());
  }
  let client = reqwest::Client::new();
  let url = String::from("https://api.themoviedb.org/3/movie/") + &id.to_string();
  println!("Fetching movie from API with url: {}", &url);
  let movie = client
    .get(&url)
    .bearer_auth(&ctx.config.tmdb_api_token)
    .send()
    .await?
    .json::<H2HMovie>()
    .await?;
  cache.insert(movie.id, movie.clone());
  Ok(movie)
}

async fn search_tmdb_movie_by_title_and_year(
  title: String,
  year: i32,
  ctx: &State<ApiContext>,
) -> Result<H2HMovie> {
  let client = reqwest::Client::new();
  let url = format!(
    "https://api.themoviedb.org/3/search/movie?query={title}&year={year}&include_adult=false"
  );
  println!("Searching for movie from API with url: {}", &url);
  let search_results: SearchMovieResponse = client
    .get(&url)
    .bearer_auth(&ctx.config.tmdb_api_token)
    .send()
    .await?
    .json::<SearchMovieResponse>()
    .await?;
  if search_results.results.len() == 0 {
    return Err(api::error::Error::NotFound);
  }
  if let Some(mov) = search_results.results.iter().find(|m| m.title == title) {
    return Ok(mov.clone());
  } else {
    return Ok(search_results.results[0].clone());
  }
}

/**
 * Fetches the top 100 movies from tmdb and return them
 */
async fn get_top100_movies(ctx: &State<ApiContext>) -> Result<Vec<H2HMovie>> {
  let mut cache = ctx.movie_cache.lock().await;
  let mut top_movies = vec![];
  let client = reqwest::Client::new();
  let mut index = 1;
  while top_movies.len() < 100 {
    let url = format!("https://api.themoviedb.org/3/discover/movie?include_adult=false&include_video=false&language=en-US&page={}&sort_by=vote_average.desc&without_genres=99,10755&vote_count.gte=10000", index);
    let response = client
      .get(&url)
      .bearer_auth(&ctx.config.tmdb_api_token)
      .send()
      .await?
      .json::<TopMoviesResp>()
      .await?
      .results;
    top_movies.extend(response);
    index += 1;
  }

  for mov in top_movies.iter() {
    cache.insert(mov.id, mov.clone());
  }

  Ok(top_movies)
}

async fn generate_h2h(
  movies: Vec<H2HMovie>,
  existing_h2h: Vec<Head2Head>,
  ctx: &State<ApiContext>,
  user_id: Uuid,
) -> Result<Vec<Head2Head>> {
  let mut head2heads: Vec<Head2Head> = Vec::new();
  if movies.len() == 0 {
    return Ok(head2heads);
  }
  let mut all_movies: Vec<H2HMovie> = movies.clone();
  while head2heads.len() < 20 {
    // choose a random movie to start
    let proponent = all_movies.choose(&mut rand::thread_rng()).unwrap();
    // get all the movies this proponent already faced before
    let existing_matchup: Vec<H2HMovie> = existing_h2h
      .iter()
      .filter(|&h2h| h2h.movie1.id == proponent.id || h2h.movie2.id == proponent.id)
      .map(|h2h| {
        if h2h.movie1.id == proponent.id {
          &h2h.movie1
        } else {
          &h2h.movie2
        }
      })
      .cloned()
      .collect();

    // remove existing matchup from the list of movies
    let filtered_movies: Vec<H2HMovie> = all_movies
      .iter()
      .filter(|&mov| mov.id != proponent.id && !existing_matchup.iter().any(|m| m.id == mov.id))
      .cloned()
      .collect();
    // if our proponent has already faced all the other movies in the list,
    // then we remove it from the list and continue the loop
    if filtered_movies.len() == 0 {
      if let Some(index) = all_movies.iter().position(|m| m.id == proponent.id) {
        all_movies.remove(index);
      }
      continue;
    }

    // find a random opponent
    let opponent = filtered_movies.choose(&mut rand::thread_rng()).unwrap();

    // create H2H
    let mut h2h: Head2Head = Head2Head {
      h2h_id: String::from("temp_id"),
      movie1: proponent.clone(),
      movie2: opponent.clone(),
    };

    // save h2h to db
    let result = sqlx::query!(
        r#"INSERT INTO "head2head" (user_id, movie1_id, movie2_id) VALUES ($1, $2, $3) RETURNING h2h_id"#,
        user_id,
        h2h.movie1.id,
        h2h.movie2.id
    )
    .fetch_one(&ctx.db)
    .await?;
    h2h.h2h_id = result.h2h_id.to_string();
    // add to list
    head2heads.push(h2h);
  }

  Ok(head2heads)
}

async fn update_elo(h2h: Head2HeadDBRow, user_id: &String, ctx: &State<ApiContext>) -> Result<()> {
  // 1. fetch both user_movies
  let user_movie_a = sqlx::query!(
    r#"SELECT user_movie_id, user_id, movie_id, elo FROM "user_movie" WHERE user_id = $1 AND movie_id = $2"#,
    Uuid::from_str(&user_id).unwrap(),
    h2h.movie1_id
  )
  .fetch_one(&ctx.db)
  .await?;

  let user_movie_b = sqlx::query!(
    r#"SELECT user_movie_id, user_id, movie_id, elo FROM "user_movie" WHERE user_id = $1 AND movie_id = $2"#,
    Uuid::from_str(&user_id).unwrap(),
    h2h.movie1_id
  )
  .fetch_one(&ctx.db)
  .await?;

  // 2. calculate expectedScore for both players
  let expected_score_a = 1f64 / (1f64 + 10f64.powf((user_movie_b.elo - user_movie_a.elo) / 400f64));
  let expected_score_b = 1f64 / (1f64 + 10f64.powf((user_movie_a.elo - user_movie_b.elo) / 400f64));
  // 3. calculate updated rating
  let movie_a_score = if user_movie_a.movie_id == h2h.winner.unwrap() {
    1f64
  } else {
    0f64
  };
  let movie_b_score = if user_movie_b.movie_id == h2h.winner.unwrap() {
    1f64
  } else {
    0f64
  };
  let new_elo_a = user_movie_a.elo + 32f64 * (movie_a_score - expected_score_a);
  let new_elo_b = user_movie_a.elo + 32f64 * (movie_b_score - expected_score_b);
  // 4. Update both scores in db
  sqlx::query!(
    r#"UPDATE "user_movie" SET elo=$1 WHERE user_id = $2 AND movie_id = $3"#,
    new_elo_a,
    Uuid::from_str(&user_id).unwrap(),
    user_movie_a.movie_id
  )
  .execute(&ctx.db)
  .await?;

  sqlx::query!(
    r#"UPDATE "user_movie" SET elo=$1 WHERE user_id = $2 AND movie_id = $3"#,
    new_elo_b,
    Uuid::from_str(&user_id).unwrap(),
    user_movie_b.movie_id
  )
  .execute(&ctx.db)
  .await?;
  Ok(())
}
