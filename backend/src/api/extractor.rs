

use crate::api::error::Error;
use crate::api::ApiContext;
use axum::async_trait;
use axum::extract::{FromRef, FromRequestParts};
use axum::http::header::AUTHORIZATION;
use axum::http::request::Parts;
use axum::http::HeaderValue;
use jsonwebtoken::{decode, encode, DecodingKey, EncodingKey, Header, Validation};
use serde::{Deserialize, Serialize};

const SCHEME_PREFIX: &str = "Bearer ";

pub struct AuthUser {
    pub user_id: String,
}


#[derive(Debug, Serialize, Deserialize)]
pub struct TokenClaims {
    pub sub: String,
    pub iat: usize,
    pub exp: usize,
}

#[async_trait]
impl<S> FromRequestParts<S> for AuthUser
where
    S: Send + Sync,
    ApiContext: FromRef<S>,
{
    type Rejection = Error;
    async fn from_request_parts(parts: &mut Parts, state: &S) -> Result<Self, Self::Rejection> {
        let ctx: ApiContext = ApiContext::from_ref(state);

        // Get the value of the `Authorization` header, if it was sent at all.
        let auth_header = parts
            .headers
            .get(AUTHORIZATION)
            .ok_or(Error::Unauthorized)?;

        Self::from_authorization(&ctx, auth_header)
    }
}

impl AuthUser {

    pub(in crate::api) fn to_jwt(&self, ctx: &ApiContext) -> String {
        let now = chrono::Utc::now();
        let iat = now.timestamp() as usize;
        let exp = get_jwt_expiry_time(now, ctx);
        let claims: TokenClaims = TokenClaims {
            sub: self.user_id.clone(),
            exp,
            iat,
        };

        encode(
            &Header::default(),
            &claims,
            &EncodingKey::from_secret(ctx.config.jwt_secret.as_ref())
        ).unwrap()
    }

    /// Attempt to parse `Self` from an `Authorization` header.
    fn from_authorization(ctx: &ApiContext, auth_header: &HeaderValue) -> Result<Self, Error> {
        let auth_header = auth_header.to_str().map_err(|_| {
            log::debug!("Authorization header is not UTF-8");
            Error::Unauthorized
        })?;

        if !auth_header.starts_with(SCHEME_PREFIX) {
            log::debug!(
                "Authorization header is using the wrong scheme: {:?}",
                auth_header
            );
            return Err(Error::Unauthorized);
        }

        let token = &auth_header[SCHEME_PREFIX.len()..];

        let claims = decode::<TokenClaims>(
            &token,
            &DecodingKey::from_secret(ctx.config.jwt_secret.as_ref()),
            &Validation::default(),
        )?.claims;

        Ok(Self {
            user_id: claims.sub,
        })
    }
}

pub fn get_jwt_expiry_time(now: chrono::DateTime<chrono::Utc>, ctx: &ApiContext) -> usize {
    let exp = (now + chrono::Duration::minutes(ctx.config.jwt_expired_in_minutes)).timestamp() as usize;
    exp
}