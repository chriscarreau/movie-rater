use clap::Parser;
use sqlx::postgres::PgPoolOptions;
use anyhow::Context;
use movie_rater::config::Config;
use movie_rater::api;

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    // dotenv::dotenv().ok();
    let config = Config::parse();

    // Initialize the logger.
    env_logger::init();

    log::info!("{:?}", config.database_url);

    let db = PgPoolOptions::new()
        .max_connections(50)
        .connect(&config.database_url)
        .await
        .context("could not connect to database")?;

    // This embeds database migrations in the application binary so we can ensure the database
    // is migrated correctly on startup
    sqlx::migrate!().run(&db).await?;

    api::serve(config, db).await?;

    Ok(())
}

