#[derive(clap::Parser)]
pub struct Config {
    /// The Postgres database host.
    #[clap(long, env)]
    pub database_url: String,

    // The TMDB API Bearer token
    #[clap(long, env)]
    pub tmdb_api_token: String,

    // JWT stuff
    #[clap(long, env)]
    pub jwt_secret: String,
    #[clap(long, env)]
    pub jwt_expired_in_minutes: i64,

}