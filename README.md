<a name="readme-top"></a>

<div align="center">
  <!-- You are encouraged to replace this logo with your own! Otherwise you can also remove it. -->
  <img src="icon.png" alt="logo" width="140"  height="auto" />
  <br/>

  <h1><b>Movie Rater</b></h1>

</div>

<!-- TABLE OF CONTENTS -->

# 📗 Table of Contents

- [📖 About the Project](#about-project)
  - [🛠 Built With](#built-with)
    - [Tech Stack](#tech-stack)
- [💻 Getting Started](#getting-started)
- [📝 License](#license)

<!-- PROJECT DESCRIPTION -->

# 📖 Movie Rater <a name="about-project"></a>

**Movie Rater** is a website that allows you to finally get a definitive answer to the  dreadful question "what's your favorite movie?".
By using a pairwise comparison algorithm,  it allows you compare all the movies you've seen against each other, and compiles a list of your favortie movies in order

## 🛠 Built With <a name="built-with"></a>

### Tech Stack <a name="tech-stack"></a>

  <summary>Client</summary>
  <ul>
    <li><a href="https://reactjs.org/">React.js</a></li>
  </ul>

  <summary>Server</summary>
  <ul>
    <li><a href="https://www.rust-lang.org/">Rust</a></li>
  </ul>

<summary>Database</summary>
  <ul>
    <li><a href="https://www.postgresql.org/">PostgreSQL</a></li>
  </ul>


<!-- GETTING STARTED -->

## 💻 Getting Started <a name="getting-started"></a>

To get a local copy up and running, run this command.

`docker compose up --build -d`


## 📝 License <a name="license"></a>

This project is [MIT](./LICENSE) licensed.


<p align="right">(<a href="#readme-top">back to top</a>)</p>
